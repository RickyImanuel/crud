@extends('adminlte.master')

@section('content')
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
              @endif
              <a class = "btn btn-primary mb-2" href="pertanyaan/create">Buat Pertanyaan Baru</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul Pertanyaan</th>
                      <th>Isi Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($post as $key => $pertanyaan)
                    <tr>
                      <td> {{$key + 1}}</td>
                      <td> {{$pertanyaan->judul}}</td>
                      <td> {{$pertanyaan->isi}}</td>
                      <td style="display: flex;"> 
                        <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                        <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                            @csrf
                            @method('Delete')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                      </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="4" style ="align: center">Tidak Ada Pertanyaan</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>

            </div>
@endsection